<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        return view('cast.index', [
            "casts" => Cast::all()
        ]);
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
//        return $request;
        $casts = $request->validate([
            "nama" => "required|max:45",
            "umur" => "required|integer",
            "bio" => "required",
        ]);

        Cast::create($casts);
        return redirect('/cast')->with('success', 'Created New Cast Successfully');
    }

    public function show ($id)
    {
        return view('cast.show', [
            "cast" => Cast::find($id)
        ]);
    }

    public function edit ($id)
    {
        return view('cast.edit', [
            "cast" => Cast::find($id)
        ]);
    }

    public function update (Request $request, $id)
    {
        // return $request;
        $casts = $request->validate([
            "nama" => "required|max:45",
            "umur" => "required|integer",
            "bio" => "required",
        ]);

        Cast::where('id', $id)->update($casts);
        return redirect('/cast')->with('success', 'Updated Cast Successfully');
    }

    public function destroy($id)
    {
        Cast::destroy($id);
        return back()->with('success', 'Cast Successfully Removed');
    }
}
