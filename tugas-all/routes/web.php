<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;


Route::view('/', 'index');
Route::view('/table', 'table');
Route::view('/data-tables', 'datatable');

Route::prefix('cast')->group(function () {
    Route::controller(CastController::class)->group(function () {
        Route::get('', 'index');
        Route::get('/create', 'create');
        Route::post('', 'store');
        Route::get('/{id}', 'show');
        Route::get('/{id}/edit', 'edit');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
    });
});
