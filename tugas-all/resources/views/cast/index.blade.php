<x-app>
    @section('container')
        <main>
            <div class="container-fluid px-4">
                <ol class="breadcrumb mb-2 mt-2">
                    <li class="breadcrumb-item active">
                        <h3>Data All Cast</h3>
                    </li>
                </ol>
                <div class="col-lg-6">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card rounded-l-full">
                            <div class="card-body">
                                <a href="{{url("/cast/create")}}" class="badge bg-success mb-2"><i class="bi bi-folder-plus"></i></a>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Age</th>
                                        <th scope="col">Bio</th>
                                        <th scope="col" class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($casts as $cast)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $cast->nama }}</td>
                                            <td>{{ $cast->umur }} Year</td>
                                            <td>{{ $cast->bio }}</td>
                                            <td class="text-center">
                                                <a href="/cast/{{ $cast->id }}" class="badge bg-info"><i class="bi bi-eye"></i></a>
                                                <a href="/cast/{{ $cast->id }}/edit" class="badge bg-warning"><i class="bi bi-pencil-square"></i></a>
                                                <form action="/cast/{{ $cast->id }}" method="POST" class="d-inline">
                                                    @method('delete')
                                                    @csrf
                                                <button type="submit" class="badge bg-danger border-0" onclick="return confirm('Are You Sure ?')">
                                                    <i class="bi bi-trash3"></i>
                                                </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection
</x-app>
