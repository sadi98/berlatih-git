<x-app>
    @section('container')
        <main>
            <div class="container-fluid px-4">
                <ol class="breadcrumb mb-2 mt-2">
                    <li class="breadcrumb-item active">
                        <h3>Create New Cast</h3>
                    </li>
                </ol>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{url("/cast")}}" method="POST">@csrf
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" value="{{ old("nama") }}" required>
                                    @error('nama')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="umur" class="form-label">Umur</label>
                                    <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" id="umur" value="{{ old("umur") }}" required>
                                    @error('umur')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="bio" class="form-label">Bio</label>
                                    <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" placeholder="Bio" id="bio" style="height: 100px" required>{{ old("bio") }}</textarea>
                                    @error('bio')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-3 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary mx-2">Save</button>
                                    <a href="{{url("/cast")}}" class="btn btn-warning">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection
</x-app>
