<x-app>
    @section('container')
        <main>
            <div class="container-fluid px-4">
                <ol class="breadcrumb mb-2 mt-2">
                    <li class="breadcrumb-item active">Edit Cast</li>
                </ol>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4>{{ $cast->nama }}</h4>
                            <h6 style="color: rgb(106, 106, 106)">{{ $cast->umur }} Year</h6>
                            <p>{{ $cast->bio }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection
</x-app>
