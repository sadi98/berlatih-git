<x-app>
    @section('container')
        <main>
            <div class="container-fluid px-4">
                <ol class="breadcrumb mb-2 mt-2">
                    <li class="breadcrumb-item active">Edit Cast</li>
                </ol>
                <div class="card">
                    <div class="card-body">
                        <form action="/cast/{{ $cast->id }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama</label>
                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" value="{{ old("nama", $cast->nama) }}" required>
                                @error('nama')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="umur" class="form-label">Umur</label>
                                <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" id="umur" value="{{ old("umur", $cast->umur) }}" required>
                                @error('umur')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="bio" class="form-label">Bio</label>
                                <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" placeholder="Bio" id="bio" style="height: 100px" required>{{ old("bio", $cast->bio) }}</textarea>
                                @error('bio')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="mb-3 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mx-2">Update</button>
                                <a href="{{url("/cast")}}" class="btn btn-warning">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    @endsection
</x-app>
