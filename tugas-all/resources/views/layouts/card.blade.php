<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card text-white mb-4 tridi" style="background: linear-gradient(0deg, rgba(34,170,195,1) 0%, rgba(120,71,255,1) 60%);">
            <div class="card-body">Primary Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card text-white mb-4 tridi" style="background: linear-gradient(0deg, rgba(255,252,24,1) 0%, rgba(255,119,0,1) 60%);">
            <div class="card-body">Warning Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card text-white mb-4 tridi" style="background: linear-gradient(0deg, rgba(0,255,175,1) 0%, rgba(0,125,16,1) 60%);">
            <div class="card-body">Success Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card text-white mb-4 tridi" style="background: linear-gradient(0deg, rgba(255,195,195,1) 0%, rgba(232,0,22,1) 74%);">
            <div class="card-body">Danger Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
</div>
