<x-app>
    @section('container')
        <main>
            <div class="container-fluid px-4">
                <ol class="breadcrumb mb-2 mt-2">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                <div class="card">
                    <div class="card-body d-flex">
                        <div class="col-xl-2 col-md-4 mx-2">
                            <div class="card text-white mb-2 tridi" style="background: linear-gradient(0deg, rgba(34,170,195,1) 0%, rgba(120,71,255,1) 60%);">
                                <div class="card-body">Tugas Pertemuan 14</div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="{{ url("/table") }}">View Details</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-4 mx-2">
                            <div class="card text-white mb-2 tridi" style="background: linear-gradient(0deg, rgba(255,252,24,1) 0%, rgba(255,119,0,1) 60%);">
                                <div class="card-body">Tugas Pertemuan 15</div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="{{ url("/cast") }}">View Details</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection
</x-app>
