<?php
require_once 'App/init.php';

$sheep = new Animal("shaun");
echo $sheep->getName();
echo $sheep->getLegs();
echo $sheep->getColdBlooded();
echo "<hr>";
$sungokong = new Ape("kera sakti");
echo $sungokong->yell();
echo "<hr>";
$kodok = new Frog("buduk");
echo $kodok->jump();