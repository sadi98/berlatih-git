<?php
class Animal {
    protected $name,
        $legs = 4,
        $cold_blooded = "no";//<== seperti ini bukan kak ??
    public function __construct($name="") {
        $this->name = $name;
    }
    public function getLegs() {
        return "Legs : {$this->legs} <br>";
    }

    public function getColdBlooded() {
        return "Cold bloode : {$this->cold_blooded} <br>";
    }

    public function getName(){
        return "Name : {$this->name} <br>";
    }
}