<?php
class Frog extends Animal {
     public  function __construct($name = "")
     {
         parent::__construct($name);
     }
    public function jump(){
        return "{$this->getName()}
                {$this->getLegs()}
                {$this->getColdBlooded()}
                Jump : Hop Hop";
    }
}